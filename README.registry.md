xd
==


[![crates.io page](https://img.shields.io/crates/v/xd)](https://crates.io/crates/xd)
[![Bitbucket Pipelines status](https://img.shields.io/bitbucket/pipelines/delan/xd/default)](https://bitbucket.org/delan/xd/addon/pipelines/home#!/results/branch/default/page/1)


- [Release notes](https://bitbucket.org/delan/xd/src/default/RELEASES.md)


xd(1) is a tool that dumps binary input in a variety of formats.


    > rustup default 1.46.0
    > cargo install xd
    > xd --example
    00 01 02 03  04 05 06 07  08 09 0A 0B  0C 0D 0E 0F  ␀☺☻♥♦♣♠•◘○◙♂♀♪♫☼  0
    10 11 12 13  14 15 16 17  18 19 1A 1B  1C 1D 1E 1F  ►◄↕‼¶§▬↨↑↓→←∟↔▲▼  10
    20 21 22 23  24 25 26 27  28 29 2A 2B  2C 2D 2E 2F   !"#$%&'()*+,-./  20
    30 31 32 33  34 35 36 37  38 39 3A 3B  3C 3D 3E 3F  0123456789:;<=>?  30
    40 41 42 43  44 45 46 47  48 49 4A 4B  4C 4D 4E 4F  @ABCDEFGHIJKLMNO  40
    50 51 52 53  54 55 56 57  58 59 5A 5B  5C 5D 5E 5F  PQRSTUVWXYZ[\]^_  50
    60 61 62 63  64 65 66 67  68 69 6A 6B  6C 6D 6E 6F  `abcdefghijklmno  60
    70 71 72 73  74 75 76 77  78 79 7A 7B  7C 7D 7E 7F  pqrstuvwxyz{|}~⌂  70
    80 81 82 83  84 85 86 87  88 89 8A 8B  8C 8D 8E 8F  ÇüéâäàåçêëèïîìÄÅ  80
    90 91 92 93  94 95 96 97  98 99 9A 9B  9C 9D 9E 9F  ÉæÆôöòûùÿÖÜ¢£¥₧ƒ  90
    A0 A1 A2 A3  A4 A5 A6 A7  A8 A9 AA AB  AC AD AE AF  áíóúñÑªº¿⌐¬½¼¡«»  A0
    B0 B1 B2 B3  B4 B5 B6 B7  B8 B9 BA BB  BC BD BE BF  ░▒▓│┤╡╢╖╕╣║╗╝╜╛┐  B0
    C0 C1 C2 C3  C4 C5 C6 C7  C8 C9 CA CB  CC CD CE CF  └┴┬├─┼╞╟╚╔╩╦╠═╬╧  C0
    D0 D1 D2 D3  D4 D5 D6 D7  D8 D9 DA DB  DC DD DE DF  ╨╤╥╙╘╒╓╫╪┘┌█▄▌▐▀  D0
    E0 E1 E2 E3  E4 E5 E6 E7  E8 E9 EA EB  EC ED EE EF  αßΓπΣσµτΦΘΩδ∞φε∩  E0
    F0 F1 F2 F3  F4 F5 F6 F7  F8 F9 FA FB  FC FD FE FF  ≡±≥≤⌠⌡÷≈°∙·√ⁿ²■   F0


The default format describes up to 16 octets on each line, in both hexadecimal and text, with the text column in one of the following character sets:


- **--table default** — a tweaked code page 437, like on an old IBM PC. This encoding provides a unique glyph for each octet, making it reversible, and almost all of those glyphs are visually distinct, which is helpful when visualising patterns in binary data that transcends printable ASCII.
- **--table classic** — printable ASCII bytes shown as exactly that, and everything else as a dot, just like conventional hex-dumping tools.
- **--table reverse** — printable ASCII bytes plus control pictures at 00h (␀) through 1Fh (␟) and 7Fh (␡), then the same glyphs in reverse video for 80h through FFh.


![screenshot for --table](https://bitbucket.org/delan/xd/raw/0.0.4/images/tables.png)


## Features


- ☑ can be consumed as a library in `#[no_std]`
- ☑ uses [pledge(2)] + [unveil(2)] on OpenBSD
- ☑ tweaked-437 + classic + reverse-video character sets
- ☑ fixed format (16×{:02X} 16×{glyph} {address})
- ☐ custom formats
- ☐ compatibility with od(1)
- ☐ compatibility with hexdump(1)/hd(1)
- ☐ compatibility with xxd(1)


[pledge(2)]: https://man.openbsd.org/OpenBSD-6.3/pledge.2
[unveil(2)]: https://man.openbsd.org/OpenBSD-6.4/unveil.2


## Requirements


- Rust 1.46+ (tested with 1.46.0)
- Should work on any platform including:
    - Linux
    - Windows 1511+
    - OpenBSD 6.4+


### Windows notes


xd(1) currently requires [ENABLE_VIRTUAL_TERMINAL_PROCESSING](https://docs.microsoft.com/en-us/windows/console/console-virtual-terminal-sequences), which was introduced on or near Windows build 10586.
This includes most versions of Windows 10 (1511+), or any version of Windows Server 2016 (1607+) or newer.


If you pipe or redirect output before it reaches the console, try **--table classic** for ASCII only, or set your code page to UTF-8.
To enable UTF-8 on Windows version 1809 or newer, run **intl.cpl**, click **Administrative**, click **Change system locale**, then check **Use Unicode UTF-8 for worldwide language support**.
If you use an older version of Windows, try [one of these methods](https://superuser.com/q/269818).
Note that all of the methods for enabling UTF-8 have been known to cause *some* software in the wild to misbehave.


For best results in conhost, don’t use Raster Fonts.
The other fonts offered by default work fairly well, with the exception of control pictures like ␀, because conhost doesn’t yet support font fallback.
For the same reason, your mileage may vary with a custom font.
