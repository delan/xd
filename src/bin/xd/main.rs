#![deny(unsafe_code)]
#![cfg_attr(feature = "strict", deny(warnings))]

#[macro_use]
mod cli;
mod platform;

use std::fs::File;
use std::io::{Cursor, Write};

use clap::value_t;
use jane_eyre::eyre::{bail, Result};
use pledge::{pledge, pledge_promises};

use xd::Dumper;

use crate::cli::{ColorOption, TableOption};
use crate::platform::{
    enable_virtual_terminal_processing, expect_pledge, expect_unveil, sigpipe_default,
};

fn main() -> Result<()> {
    sigpipe_default();
    enable_virtual_terminal_processing();

    // empty execpromises
    expect_pledge(pledge![Stdio Rpath Unveil,]);

    let arguments = crate::cli::app().get_matches();
    let input = arguments.value_of_os("input");

    input.map(|x| expect_unveil(x, "r"));
    expect_pledge(pledge_promises![Stdio Rpath]);

    jane_eyre::install()?;

    let buffering = if arguments.is_present("unbuffered") {
        // currently no reason to not use at least Line-buffered output
        Some(crate::platform::OutputBuffering::Line)
    } else {
        None
    };

    // currently no reason to ever not use Full-buffered input
    let mut input = if arguments.is_present("example") {
        let sweep: Vec<_> = (0x00..=0xFF).collect();
        crate::platform::input(None, Cursor::new(sweep))
    } else if let Some(path) = input {
        crate::platform::input(None, File::open(path)?)
    } else {
        crate::platform::stdin_binary(None)
    };

    expect_pledge(pledge_promises![Stdio]);

    let mut output = crate::platform::stdout_text(buffering);

    let table = value_t_default_exit!(arguments, "table", TableOption).into();
    let color = value_t_default_exit!(arguments, "color", ColorOption).should();
    let mut dumper = Dumper::new(table, color);

    if table.requires_rich && !color {
        bail!("color is unavailable but required by this --table (see --help for --color)");
    }

    while dumper.read(&mut input)? > 0 {
        write!(output, "{}", dumper)?;
    }

    Ok(())
}
