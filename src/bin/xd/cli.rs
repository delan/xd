use std::env::var_os;

use atty::{self, Stream};
use clap::{arg_enum, App, Arg};

use xd::table::*;

macro_rules! value_t_default_exit {
    ($m:ident, $v:expr, $t:ty) => {
        match ::clap::value_t!($m, $v, $t) {
            Err(::clap::Error {
                kind: ::clap::ErrorKind::ArgumentNotFound,
                ..
            }) => <$t as ::core::default::Default>::default(),
            Err(other) => other.exit(),
            Ok(other) => other,
        }
    };
}

arg_enum! {
    #[derive(Debug)]
    #[allow(non_camel_case_types)]
    pub enum TableOption {
        default,
        classic,
        reverse,
    }
}

impl Default for TableOption {
    fn default() -> Self {
        Self::default
    }
}

arg_enum! {
    #[derive(Debug)]
    #[allow(non_camel_case_types)]
    pub enum ColorOption {
        auto,
        never,
        always,
    }
}

impl ColorOption {
    pub fn should(&self) -> bool {
        match self {
            Self::never => return false,
            Self::always => return true,
            Self::auto => (),
        }

        if !atty::is(Stream::Stdout) {
            return false;
        }

        let missing_term_is_meaningful = cfg!(unix);

        if var_os("TERM").map_or(missing_term_is_meaningful, |x| x == "dumb") {
            return false;
        }

        // “preference” environment variables not yet enabled, because i’m not yet convinced that
        // either of these conventions is accepted widely enough to commit to permanent support

        #[cfg(any())]
        if var("CLICOLOR_FORCE").as_ref().map(|x| &**x).unwrap_or("0") != "0" {
            return true;
        }

        #[cfg(any())]
        if let Ok("0") = var("CLICOLOR").as_ref().map(|x| &**x) {
            return false;
        }

        #[cfg(any())]
        if var("NO_COLOR").is_ok() {
            return false;
        }

        return true;
    }
}

impl Default for ColorOption {
    fn default() -> Self {
        Self::auto
    }
}

impl From<TableOption> for &xd::table::Table {
    fn from(variant: TableOption) -> Self {
        match variant {
            TableOption::default => &DEFAULT,
            TableOption::classic => &CLASSIC,
            TableOption::reverse => &REVERSE,
        }
    }
}

#[rustfmt::skip]
pub fn app() -> App<'static, 'static> {
    App::new("xd")
        .version(clap::crate_version!())
        .author(clap::crate_authors!("\n"))
        .about("
Dumps binary input in a variety of formats.

The default format describes up to 16 octets on each line, in both hexadecimal
and text, and the latter with a tweaked code page 437, like on an old IBM PC.

This encoding provides a unique glyph for each octet, making it reversible,
and almost all of those glyphs are visually distinct, which is helpful when
visualising patterns in binary data that transcends printable ASCII.

Use -h for short descriptions and --help for more details.

Project home page: <https://bitbucket.org/delan/xd>
")
        .arg(Arg::from_usage("--example 'Use an example as input'").display_order(0))
        .arg(Arg::from_usage("[color] --color [when] 'When to use colours or other text styles\n'")
            .case_insensitive(true)
            .possible_values(&ColorOption::variants())
            // FIXME if only clap had a hide_possible_values_long_help
            .hide_possible_values(true)
            .long_help(concat!("When to use colours or other text styles:
  • auto        [default] try to be smart
  • never       never use colours or other text styles
  • always      always use colours or other text styles

In auto mode, we decide whether or not to use them as follows:
  • no, if standard output is not a tty (Unix) or console (Windows)
  • no, if the environment has TERM=dumb (all platforms)
  • no, if the environment has no TERM (Unix)
", /* "  • yes, if the environment has CLICOLOR_FORCE≠0
",    "  • no, if the environment has CLICOLOR=0 or NO_COLOR
", */ "  • yes, otherwise
", /* FIXME why does clap trim this line off, when it leaves a blank line after other options? */ " ")))
        .arg(Arg::from_usage("[table] --table [name] 'Character set to use for text columns\n'")
            .case_insensitive(true)
            .possible_values(&TableOption::variants())
            // FIXME if only clap had a hide_possible_values_long_help
            .hide_possible_values(true)
            .long_help(concat!("Character set to use for text columns:
  • default     code page 437 (full graphics) + symbol for null
  • classic     printable ASCII + dot (.) for everything else
  • reverse     control pictures + reverse video for high octets
", /* FIXME why does clap trim this line off, when it leaves a blank line after other options? */ " ")))
        .arg(Arg::from_usage("--unbuffered 'Flush output after each line'").display_order(0).alias("line-buffered"))
        .arg(Arg::from_usage("[input] 'Input file path [default: standard input]'"))
}
