#![allow(unsafe_code)]

use std::ffi::OsStr;
use std::fs::File;
use std::io;
use std::io::LineWriter;
use std::io::{BufReader, Read};
use std::io::{BufWriter, Write};

use atty::{self, Stream};

/// Allow SIGPIPE to terminate the process.
pub fn sigpipe_default() {
    // FIXME non-POSIX platforms?
    #[cfg(unix)]
    unsafe {
        // https://github.com/rust-lang/rust/issues/46016#issuecomment-428106774
        // https://github.com/rust-lang/rust/issues/46016#issuecomment-605624865
        use nix::sys::signal::{signal, SigHandler::SigDfl, Signal::SIGPIPE};
        signal(SIGPIPE, SigDfl).expect("signal(2) failed");
    }
}

/// Enable ANSI escape codes on Windows.
pub fn enable_virtual_terminal_processing() {
    #[cfg(windows)]
    if atty::is(Stream::Stdout) {
        winapi_util::console::Console::stdout()
            .expect("GetConsoleScreenBufferInfo failed on stdout")
            .set_virtual_terminal_processing(true)
            .expect("GetConsoleMode or SetConsoleMode failed on stdout");
    }
    #[cfg(windows)]
    if atty::is(Stream::Stderr) {
        winapi_util::console::Console::stderr()
            .expect("GetConsoleScreenBufferInfo failed on stderr")
            .set_virtual_terminal_processing(true)
            .expect("GetConsoleMode or SetConsoleMode failed on stderr");
    }
}

pub fn expect_pledge(result: Result<(), pledge::Error>) {
    result
        .or_else(pledge::Error::ignore_platform)
        .expect("pledge(2) failed");
}

#[cfg(not(unix))]
pub fn expect_unveil(_: impl AsRef<OsStr>, _: &str) {}

#[cfg(unix)]
pub fn expect_unveil(path: impl AsRef<OsStr>, permissions: &str) {
    use std::os::unix::ffi::OsStrExt;
    match unveil::unveil(path.as_ref().as_bytes(), permissions) {
        Err(unveil::Error::NotSupported) => Ok(()),
        x => x,
    }
    .expect("unveil(2) failed");
}

trait AsFile {
    fn as_file(&self) -> File;
}

/// Local buffering mode for an input.
///
/// There’s no way to locally line-buffer input when running on a typical operating system. The
/// line buffering you might see when stdin is a tty is provided by the tty’s “line discipline”.
/// See <https://www.linusakesson.net/programming/tty/> for more details.
///
/// You probably don’t want Raw unless your program’s functions include exiting of its own volition
/// after reading a finite amount of input, in which case allowing the user to choose Raw would be a
/// useful way to avoid claiming input destined for a subsequent process.
/// See <http://www.pixelbeat.org/programming/stdio_buffering/> for more details.
///
/// Either way, Full is usually the better default.
pub enum InputBuffering {
    #[allow(dead_code)]
    Raw,
    Full,
}

/// Local buffering mode for an output.
pub enum OutputBuffering {
    #[allow(dead_code)]
    Raw,
    Line,
    Full,
}

impl Default for InputBuffering {
    fn default() -> Self {
        InputBuffering::Full
    }
}

impl Default for OutputBuffering {
    fn default() -> Self {
        OutputBuffering::Full
    }
}

impl OutputBuffering {
    fn default(stream: impl Into<Option<Stream>>) -> Self {
        if stream.into().map_or(false, |x| atty::is(x)) {
            OutputBuffering::Line
        } else {
            OutputBuffering::Full
        }
    }
}

lazy_static::lazy_static! {
    static ref STDIN: io::Stdin = io::stdin();
    static ref STDOUT: io::Stdout = io::stdout();
}

fn stdin0() -> File {
    STDIN.as_file()
}

fn stdout0() -> File {
    STDOUT.as_file()
}

/// Opens an independent stdin, with appropriate buffering depending on isatty(3) or similar.
pub fn stdin_binary(buffering: Option<InputBuffering>) -> Box<dyn Read> {
    input(buffering, stdin0())
}

/// Opens an independent stdout, with appropriate buffering depending on isatty(3) or similar.
///
/// This works around a Rust limitation where StdoutLock is [currently always a LineWriter][0].
///
/// If you plan on writing text to stdout, consider using [`stdout_text`] instead, whose behaviour
/// around Unicode and consoles is better on Windows.
///
/// [0]: https://doc.rust-lang.org/1.46.0/src/std/io/stdio.rs.html#518
pub fn stdout_binary(buffering: Option<OutputBuffering>) -> Box<dyn Write> {
    output(
        Some(buffering.unwrap_or_else(|| OutputBuffering::default(Stream::Stdout))),
        stdout0(),
    )
}

/// Opens an independent stdout, with appropriate buffering depending on isatty(3) or similar, and
/// guaranteed Unicode console output on Windows.
///
/// On Windows, when stdout is a console (roughly speaking, when it isn’t piped or redirected), we
/// can use WriteConsoleW to print Unicode, regardless of the console’s code page. This eliminates
/// the need for users to chcp 65001 (unless they pipe and filter our output).
///
/// Correctly using WriteConsoleW is easier said than done, so we use [the implementation in Rust’s
/// standard library][0], which is currently only exposed in std::io::Stdout (via inner, which is
/// effectively a `LineWriter<std::sys::windows::stdio::Stdout>`).
///
/// [0]: https://github.com/rust-lang/rust/blob/068320b39e3e4839d832b3aa71fa910ba170673b/library/std/src/sys/windows/stdio.rs
pub fn stdout_text(buffering: Option<OutputBuffering>) -> Box<dyn Write> {
    #[cfg(windows)]
    if atty::is(Stream::Stdout) {
        match buffering.unwrap_or_else(|| OutputBuffering::default(Stream::Stdout)) {
            OutputBuffering::Raw => unimplemented!(),
            OutputBuffering::Line => return Box::new(STDOUT.lock()),
            OutputBuffering::Full => unimplemented!(),
        }
    }

    stdout_binary(buffering)
}

pub fn input(buffering: Option<InputBuffering>, inner: impl Read + 'static) -> Box<dyn Read> {
    match buffering.unwrap_or_default() {
        InputBuffering::Raw => Box::new(inner),
        InputBuffering::Full => Box::new(BufReader::new(inner)),
    }
}

pub fn output(buffering: Option<OutputBuffering>, inner: impl Write + 'static) -> Box<dyn Write> {
    match buffering.unwrap_or_default() {
        OutputBuffering::Raw => Box::new(inner),
        OutputBuffering::Line => Box::new(LineWriter::new(inner)),
        OutputBuffering::Full => Box::new(BufWriter::new(inner)),
    }
}

#[cfg(unix)]
mod platform {
    use std::fs::File;

    use std::os::unix::io::{AsRawFd, FromRawFd};

    impl<T: AsRawFd> super::AsFile for T {
        fn as_file(&self) -> File {
            let fd = self.as_raw_fd();

            unsafe { File::from_raw_fd(fd) }
        }
    }
}

#[cfg(windows)]
mod platform {
    use std::fs::File;

    use std::os::windows::io::{AsRawHandle, FromRawHandle};

    impl<T: AsRawHandle> super::AsFile for T {
        fn as_file(&self) -> File {
            let handle = self.as_raw_handle();

            unsafe { File::from_raw_handle(handle) }
        }
    }
}
