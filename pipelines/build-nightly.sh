#!/bin/sh

set -eu

. pipelines/step.sh
. pipelines/versions.sh

# we only check formatting in build-1.46.sh because rustfmt’s style varies across versions

step 'run tests with strict' rustup run "$toolchain_nightly" \
    cargo test --features strict --all-features --all-targets --workspace

step 'doc tests with strict' rustup run "$toolchain_nightly" \
    cargo test --features strict --all-features --doc --workspace

step 'check #![no_std]' rustup run "$toolchain_nightly" \
    cargo check --no-default-features --lib
