#!/bin/sh
# usage: pipelines/build-1.46.sh [coverage [upload]]

set -eu

. pipelines/step.sh
. pipelines/versions.sh

step 'check formatting' rustup run "$toolchain_1_46" \
    cargo fmt --all -- --check

step 'run tests with strict' rustup run "$toolchain_1_46" \
    cargo test --features 'strict std' --all-targets --workspace

step 'doc tests with strict' rustup run "$toolchain_1_46" \
    cargo test --features 'strict std' --doc --workspace

step 'check #![no_std]' rustup run "$toolchain_1_46" \
    cargo check --no-default-features --lib

if [ $# -gt 0 ] && [ _"$1" = _'coverage' ]; then
    step 'calculate code coverage' rustup run "$toolchain_1_46" \
        cargo tarpaulin --features 'strict std' --all --out Xml

    if [ $# -gt 1 ] && [ _"$2" = _'upload' ]; then
        upload() {
            codecov=$(mktemp)
            curl -fsSo $codecov https://codecov.io/bash
            chmod +x $codecov
            $codecov
        }

        step 'upload code coverage' upload
    fi
fi
