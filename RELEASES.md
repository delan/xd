# ?.?.? (????-??-??)


- xd(1): accept binary or non-UTF-8 input paths on OpenBSD ([asmarques/unveil-rs#2](https://github.com/asmarques/unveil-rs/pull/2))


# 0.0.4 (2020-11-29)


- xd(1): fixed cargo install --locked on OpenBSD by bumping dependencies.nix to 0.19.1
- \[lib]: marked dependencies only needed by xd(1) as optional (skipped for \[lib])


# 0.0.3 (2020-11-23)


- **--table default** ([DEFAULT](https://docs.rs/xd/0.0.3/xd/table/constant.DEFAULT.html)) now uses bold and dimmed to enhance contrast ([h/t @b_](https://twitter.com/b_/status/1328459231918481409))
- improved build performance by moving the README.registry.md generator from build.rs to tests/registry.rs
- added Windows compatibility notes


## xd(1)


- added support for disabling ANSI escapes based on a new **--color** option + isatty(3) + TERM
    - **--table reverse** is now a fatal error when ANSI escapes are unavailable (for now at least)
- fixed bug where ANSI escapes didn’t work on Windows (issue #1)
- improved I/O performance:
    - input is now always fully-buffered, because xd(1) provides no way to limit how much it reads either way
    - **--unbuffered** now flushes after each line, because [Dumper](https://docs.rs/xd/0.0.3/xd/struct.Dumper.html) wouldn’t benefit from more frequent writes
    - added **--line-buffered** as an alias for **--unbuffered**, for the same reason


## \[lib]


- [crate::Dumper](https://docs.rs/xd/0.0.3/xd/struct.Dumper.html):
    - added a `use_rich` parameter to `new` (previously effectively true)
- [crate::table::Table](https://docs.rs/xd/0.0.3/xd/table/struct.Table.html):
    - changed from a type alias to a struct with an `inner`
    - added `requires_rich` field (true for [crate::table::REVERSE](https://docs.rs/xd/0.0.3/xd/table/constant.REVERSE.html))
- [crate::table::Reversible](https://docs.rs/xd/0.0.2/xd/table/struct.Reversible.html) is now [RichScalar](https://docs.rs/xd/0.0.3/xd/table/struct.RichScalar.html):
    - changed from a tuple struct to an ordinary struct (0 → `scalar`, 1 → `reversed`)
    - added `bold` and `dimmed` flags


# 0.0.2 (2020-11-16)


- xd(1): fixed bug where --table was required


# 0.0.1 (2020-11-16)


- added support for custom tables with optional reverse video ([h/t @bazzargh](https://twitter.com/dazabani/status/1328193107930673153))
    - xd(1): `--table <default|classic|reverse>`
    - \[lib]: xd::table::{[Table](https://docs.rs/xd/0.0.1/xd/table/type.Table.html), [Reversible](https://docs.rs/xd/0.0.1/xd/table/struct.Reversible.html)}
- added note about installing on OpenBSD


# 0.0.0 (2020-11-15)


- initial release
